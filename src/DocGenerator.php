<?php

namespace ApiDoc;

use Exception;
use Illuminate\Support\Facades\Storage;

class DocGenerator
{
    const structure = [
        "tags" => [],
        "requests" => []
    ];
    private $request_model = [
        "tag" => "",
        "description" => "",
        "method" => "",
        "uri" => "",
        "request_headers" => [],
        "request_body" => [],
        "status" => 0,
        "response_body" => []
    ];
    private $re = '/(?<=[a-z])(?=[A-Z])/x';
    private $data = [];
    private $file = [];


    public function __construct(array $arr = [])
    {
    }

    public function init()
    {
        if (!Storage::disk('local')->exists('docs.html')) {
            $template = file_get_contents(__DIR__ . "/template_01.html");
            Storage::disk('public')->put('docs.html', $template);
        }
        Storage::disk('public')->put('docs.json', json_encode(DocGenerator::structure));
    }

    public function make($name, $request, $response)
    {
        // return new self($name, $request, $response);
        $this->resolveName($name);
        $this->makeRequestStructure($request, $response);
        $this->updateFile();
    }


    private function resolveName($name)
    {
        $this->file = json_decode(Storage::get('public/docs.json'), true);
        $result = preg_split($this->re, $name);
        $tags = $this->file["tags"];
        array_push($tags, $result[1]);
        $this->file["tags"] = array_unique($tags);
        $this->data = $result;
    }
    private function makeRequestStructure($request, $response)
    {
        $this->request_model["tag"] = $this->data[1];
        $this->request_model["description"] = implode(" ", array_slice($this->data, 2));
        $this->request_model["method"] = $request["method"];
        $this->request_model["uri"] = $request["uri"];
        $this->request_model["request_headers"] = $request["headers"];
        $this->request_model["request_body"] = $request["body"];
        $this->request_model["status"] = $response->status();
        $this->request_model["response_body"] = $response->getOriginalContent();
    }

    private function updateFile()
    {
        $r = array_walk_recursive($this->request_model, function (&$x) {
            if (gettype($x) == "object") {
                try {
                    $x->toArray();
                } catch (Exception $e) {
                    $x = "[Object]";
                }
            }
        });
        $requests = $this->file["requests"];
        array_push($requests, $this->request_model);
        $this->file["requests"] = $requests;
        $this->file = json_encode($this->file);
        Storage::disk('public')->put('docs.json', $this->file);
    }
}
