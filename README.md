
# laravel-apidoc

Generate API documentation from test cases(for Laravel).

# Installation
~~~
composer require cybersrikanth/laravel-apidoc
~~~
### Usage & Notes

* This package should be registered as service in your Laravel application.
* Add the following line in your `AppServiceProvider.php`'s `register` method.
~~~
$this->app->singleton('DocGen', 'ApiDoc\DocGenerator');
~~~
* This will generate `docs.json` and `docs.html` under `storage/app/public`. So you have to run the following command to serve the generated docs.
~~~
php artisan storage:link
~~~
* You should use proper camel case names for test case methods. The title and description will be generated from the function name. (you can override this)\

    Example:\
        `public function testLeaveApplyWithoutAttachment()`

    In this function name, 1st-word test will be ignored \
    2nd-word Leave will be title\
    3rd to nth words will be the description

* You should use proper request structure as given below. Paste the following code in `TestCase.php`
~~~
    protected $request = [
        "method" => "GET",
        "uri" => "",
        "headers" => ["Accept" => "application/json", "Content-Type" => "application/json"],
        "body" => []
    ];
~~~
* you can access the request object from any of your test case class.

<ins>Example:</ins> \
&nbsp; $this->request["method"] =  "POST";\
&nbsp; $this->request["uri"] = "api/v1/auth/login";\
&nbsp; $this->request["body"] = [\
&nbsp;   &nbsp; &nbsp;  'name'  => 'user',\
&nbsp;   &nbsp; &nbsp;  'password' => 'password',\
&nbsp; ]

* You can add methods to manipulate the request format, that you may wanna use frequently. Add the bellow code in `TestCase.php`.
~~~
    protected function setRequestHeader($key, $value)
    {
        $headers = $this->request["headers"];
        $headers[$key] = $value;
        $this->request['headers'] = $headers;
    }

    protected function fire()
    {
        return $this->call($this->request["method"], $this->request["uri"], $this->request["body"], $this->request["headers"]);
    }
~~~
* The above method `setRequestHeader()` accepts key and value. you can call this from any of your test case classes.\
Eg:\
 `$this->setRequestHeader("ContentType", "multipart/form-data"); `
* `fire()` method simply triggers http request with the data you inserted in request array.\
Eg:\
 `$this->fire()` will trigger http request.
* Since we are generating documentation from test cases, we need to clean documentation generated on the previous run to avoid duplicate entries. Paste the following code in `TestCase.php` class.
~~~
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->setUp();
        app('DocGen')->init(); // this method clears previously generated docs.
    }

    public function setUp(): void
    {
        parent::setUp();
        // Uncomment bellow line if you want to use passport fake for authentication.
        // Artisan::call('passport:install'); 
~~~
* Insert the following line at end of the test case methods that you want to generate docs.
~~~
    app('DocGen')->make(__FUNCTION__, $this->request, $response);
~~~
* If you want to use long description (not from function name), simply replace `__FUNCTION__` with the custom name followed by the convention we mentioned above (Eg: `testTitleLongDescriptionYouWant`).
* Run the test case. Your documentation will be generated and published in (http://localhost:8000/storage/docs.html)

